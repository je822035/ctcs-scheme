"""
28822035
"""

import numpy as np
import matplotlib.pyplot as plt

def initConditions(x):
    "First set of ICs"
    
    nx = len(x)
    phi1 = np.zeros(nx) # First set
    phi2 = phi1.copy() # Second set
    
    for j in range(nx):
        if x[j] >= 0 and x[j] < 1/2:
            phi1[j] = (1 - np.cos(4*np.pi*x[j]))/2
            phi2[j] = 1
    
    return phi1, phi2

# SQUARE WAVE INIT CONDITIONS STILL TO DO

def CTCS(phiOld, c, nt, nx):
    """"""
    
    # new time-step array for phi
    phi = phiOld.copy()
    
    # FTCS for first time-step
    for j in range(0, nx):
        phi[j] = phiOld[j] - c*(phiOld[(j+1)%nx] - phiOld[(j-1)%nx])/2
    
    phiNew = phi.copy()
    
    # FTCS over all time-steps
    for it in range(1, nt):
        # Set the zero gradient boundary conditions
        for j in range(0, nx):
            phiNew[j] = phiOld[j] - c*(phi[(j+1)%nx] - phi[(j-1)%nx])
            
        phi = phiNew.copy()
        phiOld = phi.copy()
    
    return phiNew

def main():
    
    # Paramaters
    xmin = 0
    xmax = 1
    nx = 40
    dx = (xmax - xmin)/nx
    c = 0.1
    u = 1
    nt = 50
    T = 0.125
    dt = T/nt
    print(u*dt/dx)
    print(dx, dt)
    
    # Define x
    x = np.zeros(nx)
    for i in range(nx):
        x[i] = i*dx
    
    # ICs
    phi1, phi2 = initConditions(x)
    
    # Exact solutions
    adv_exact1, adv_exact2 = initConditions((x - u*T) % (xmax - xmin))
    
    # CTCS for both ICs
    adv_phi1 = CTCS(phi1, c, nt, nx)
    adv_phi2 = CTCS(phi2, c, nt, nx)
    
    # Plot
    plt.clf()
    plt.plot(x, phi1, label = "ICs")
    plt.plot(x, adv_exact1, label = "exact")
    plt.plot(x, adv_phi1, label = "CTCS")
    plt.legend()
    #plt.plot(x, adv_exact2)
    
main()
    
    
    
