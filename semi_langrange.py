"""
28822035
"""

from math import *

def Semi_Lagrange(phiOld, c, nt):
    """"""
    
    nx = len(phiOld) # number of spatial steps
    
    phi = phiOld.copy()
    
    for i in range(nt):
        
        for j in range(nx):
            # grid index
            k = floor(j - c)
        
            b = j - k - c # for interpolation
            
            # run the interpolation to find
            phi[j] = - b*(1-b)*(2-b)/6 *phiOld[(k-1) % nx] \
                    + (1+b)*(1-b)*(2-b)/2 *phiOld[k % nx] \
                    + (1+b)*b*(2-b)/2 *phiOld[(k+1) % nx] \
                    - (1+b)*b*(1-b)/6 *phiOld[(k+2) % nx]
            
            phiOld = phi.copy()
    
    return phi