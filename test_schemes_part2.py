"""
28822035
Main Function
"""

import numpy as np
import matplotlib.pyplot as plt

from semi_langrange import *
from advectionICs import *

def main():
    
    # Paramaters
    xmin = 0
    xmax = 1
    nx = 40 # number of points in space
    dx = (xmax - xmin)/nx # spatial step
    u = 1 # speed
    nt = 50 # number of time-steps
    T = 0.125  # duration
    
    # Derived parameters
    dt = T/nt # time-step
    c = u*dt/dx # Courant number
    
    # Define x
    x = np.zeros(nx)
    for i in range(nx):
        x[i] = i*dx
    
    # FIRST INITIAL CONDITION
    phiCos = initCos(x)
   
    # Exact solutions
    exactCos = initCos((x - u*T) % (xmax - xmin))
    
    #
    lagrangeCos = Semi_Lagrange(phiCos, c, nt)
   
    
    # CTCS not visible as very close to CNCS change this!
    # Plot 1st Initial Condition
    plt.clf()
    plt.plot(x, phiCos, 'k--', label = "ICs")
    plt.plot(x, exactCos, 'k',label = "exact")
    plt.plot(x, lagrangeCos, '*-', label = "Lagrange")
    plt.title("Initial Condition = (1 - cos(4*pi*x))/2")
    plt.xlabel("x")
    plt.ylabel("phi")
    plt.legend()
    #plt.savefig("advectionSchemesIC1.pdf")
    plt.show()
    
    # SECOND INITIAL CONDITION
    phiSquare = initSquare(x)
    exactSquare = initSquare((x - u*T) % (xmax - xmin))
    lagrangeSquare = Semi_Lagrange(phiSquare, c, nt)
    
    # Plot 2nd Initial Condition
    plt.clf()
    plt.plot(x, phiSquare, 'k--', label = "ICs")
    plt.plot(x, exactSquare, 'k', label = "exact")
    plt.plot(x, lagrangeSquare, '*-', label = "Lagrange")
    plt.title("Initial Condition = 1")
    plt.xlabel("x")
    plt.ylabel("phi")
    plt.legend()
    #plt.savefig("advectionSchemesIC2.pdf")
    plt.show()
    
main()
    
    
    
